from flask import Flask, jsonify, request
import base64
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import re
import pandas as pd
from scipy.sparse import csr_matrix
from sys import exit as e
import numpy as np
import operator

app = Flask(__name__)


@app.route('/<string:name>')
def index(name: str):
    # name=request.args.get("name")
    # sample_string = name
    # sample_string_bytes = sample_string.encode("ascii")
    #
    # base64_bytes = base64.b64encode(sample_string_bytes)
    # raw = base64_bytes.decode("ascii")

    base64_string = name
    base64_bytes = base64_string.encode("ascii")

    sample_string_bytes = base64.b64decode(base64_bytes)
    raw = sample_string_bytes.decode("ascii")

    a = set_kelas(raw)

    # U2FsYW0sCgogICAgU2F5YSBzZWRhbmcgbWVuY2FyaSBkdWEgcHVibGlrYXNpIGRhdGEgSW5wdXQgT3V0cHV0IHRlcmtpbmkgeWFuZyBkaW1pbGlraSBCUFMgdW50dWsgUHJvdmluc2kgQmFsaSwgTlRCLCBkYW4gTlRULiBEYXRhIGluaSBkaXBlcmx1a2FuIGtodXN1c255YSB1bnR1ayBwZW5lbGl0aWFuIGRpIHNla3RvciBwYXJpd2lzYXRhLCBzZWhpbmdnYSBidXR1aCBrZXRpZ2Egd2lsYXlhaCB0ZXJzZWJ1dCB1bnR1ayBtZWxpaGF0IGludGVycmVnaW9uYWwgbXVsdGlwbGllciBlZmZlY3QtbnlhLiBNb2hvbiBkaWluZm9rYW4gcHVsYSBwYWJpbGEgdGVyZGFwYXQgZGF0YSBkaSBsZXZlbCBrYWJ1cGF0ZW4geWFuZyB0ZXJzZWRpYS4gCgogICAgSmlrYSBtZW11bmdraW5rYW4sIHNheWEgbW9ob24gaW5mb3JtYXNpbnlhIGRhbGFtIGt1cnVuIHdha3R1IGhhcmkgaW5pIGF0YXUgYmVzb2suIFNheWEgYmlzYSBkaWh1YnVuZ2kgdmlhIGVtYWlsIG1hdXB1biB2aWEgc21zIHBhZGEgbm9tb3IgcG9uc2VsIHlhbmcgdGVydGVyYSBkaSBwcm9maWwgc2F5YS4KCiAgICBUZXJpbWEga2FzaWg=

    if a :
        return jsonify(data=a), 200
    else :
        return jsonify(data="xxxx"), 400


def set_kelas(kalimat):
    cl = cleaning(kalimat)
    cl = cleanhtml(cl)
    cl = cl.replace('&amp;', '&')
    cl = re.sub('&lt;/?[a-z]+&gt;', '', cl)

    # return jsonify(data=cl), 200

    df = pd.read_csv("wil_rev.csv")

    df_list = list(df.kata_clean)

    df_list.append(cl)

    # count_vectorizer = CountVectorizer(tokenizer=tokenize)
    # data = count_vectorizer.fit_transform(train_set).toarray()
    # vocab = count_vectorizer.get_feature_names()

    tfidf = TfidfVectorizer().fit_transform(df_list)

    pairwise_similarity = tfidf * tfidf.T

    # ss = pairwise_similarity[34, :]
    b = {}

    # return cl

    for i in range(34):
        b[i] = pairwise_similarity[34, i]
    max_v = max(b.items(), key=operator.itemgetter(1))[0]

    hd = ""

    if max_v > 0:
        # return max_v
        fg = df.iloc[max_v, [1]]
        hd = fg[0]
        return str(hd)
    else:
        # hd =
        return '0000'
    return str(hd), b[max_v]


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def cleaning(kalimat):
    tandabaca = [".", ",", "-", "%", "?"]
    for td in tandabaca:
        kalimat = kalimat.replace(td, "")
    stop_list = open("katadasar.txt", "r")
    stop_kata = stop_list.readlines()
    list_stop = [l.strip() for l in stop_kata]
    da = []
    kalimat = kalimat.split()

    for item in kalimat:
        item = item.lower().strip()
        if item in list_stop:
            continue
        else:
            da.append(item)
    return " ".join(da)


if __name__ == "__main__":
    app.run()
